import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OpenFoodFactProduct } from '../entity/open-food-fact-product.vo';
import { FavService } from '../services/fav.service';
import { OffService } from '../services/off.service';

@Component({
  selector: 'app-result-view',
  templateUrl: './result-view.component.html',
  styleUrls: ['./result-view.component.css']
})
export class ResultViewComponent implements OnInit {

  constructor( private route: ActivatedRoute, private offService : OffService, private favService : FavService) { }

  barCode : string | undefined;

  openFoodFactProduct: OpenFoodFactProduct | undefined;

  nutriColor: string | undefined;
  nutriLabel: string | undefined;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.codebar) {
        this.barCode = params.codebar;
        this.searchOff();
      }
    });
  }

  async searchOff(){
    if(this.barCode != null && this.barCode.trim().length > 0){
      this.openFoodFactProduct = await this.offService.searchBarCode(this.barCode);
      console.info("openFoodFactProduct ", this.openFoodFactProduct)
      this.setNutrimentColor(this.openFoodFactProduct as OpenFoodFactProduct)
    }
  }

  private setNutrimentColor(openFoodFactProduct: OpenFoodFactProduct): void {
    let color = null;
    let score = 100;
   
    if (openFoodFactProduct && openFoodFactProduct.product?.nutrient_levels) {
      let prop: string;
      for (prop in openFoodFactProduct.product?.nutrient_levels) {
        if (openFoodFactProduct.product?.nutrient_levels[prop]) {
          const val: string = openFoodFactProduct.product?.nutrient_levels[prop];

          if (val === "fat") {
            score -= 30;
          } else if (val === "high") {
            score -= 20;
          } else if (val === "moderate") {
            score -= 10;
          } else if (val === "low") {
            score += 0;
          }
        }
      }
    }

    if (score >= 80) {
      color = "green";
      this.nutriLabel = `${score} / 100 : Bon`;
    } else if (score >= 50) {
      color = "orange";
      this.nutriLabel = `${score} / 100 : Moyen`;
    } else if (score >= 20) {
      color = "red";
      this.nutriLabel = `${score} / 100 : Médiocre`;
    } else {
      color = "black";
      this.nutriLabel = `${score} / 100 : Interdit`;
    }

    this.nutriColor = `nutri-container ${color}`;
  }

  favClickHandler(){
    if (this.openFoodFactProduct) {
      this.openFoodFactProduct.fav = !this.openFoodFactProduct.fav;
      this.favService.addFavProduct(
        this.openFoodFactProduct,
        this.openFoodFactProduct.fav
      );
    }
  }
}
