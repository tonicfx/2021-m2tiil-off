import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Result, BarcodeFormat } from "@zxing/library";
import { ZXingScannerComponent } from "@zxing/ngx-scanner";
import { OffService } from '../services/off.service';

@Component({
  selector: 'app-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.css']
})
export class SearchViewComponent implements OnInit {
  @ViewChild("scanner", { static: true })
  scanner: ZXingScannerComponent | undefined;

  constructor(private router: Router) { }

  public barCodeValue : String | undefined;

  public allowedFormats : Array<number> = [ BarcodeFormat.QR_CODE, BarcodeFormat.EAN_13, BarcodeFormat.CODE_128, BarcodeFormat.DATA_MATRIX ];

  ngOnInit(): void {
    this.scanner!.camerasFound.subscribe((devices: MediaDeviceInfo[]) => {
      console.info("devices ", devices);
      if (devices && devices.length > 0) {
        const index = devices.length > 1 ? 1 : 0;
        this.scanner!.autostart = true;
        this.scanner!.device = devices[0];
        this.scanner!.restart();
      }
    });
  }

  searchBarCodeHandler(event:any) {
    this.searchOff();
  }
  scanQRCodeCompleteHandler(qrCode:any) {
    this.barCodeValue = qrCode
    this.searchOff();
  }

  searchOff() {
    console.info("searchOff :",this.barCodeValue);
    this.router.navigate(["/result-view/:codebar", { codebar: this.barCodeValue }]);
  }
}
