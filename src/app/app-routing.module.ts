import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavViewComponent } from './fav-view/fav-view.component';
import { ResultViewComponent } from './result-view/result-view.component';
import { SearchViewComponent } from './search-view/search-view.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "search-view",
    pathMatch: "full"
  },
  { path: "search-view", component: SearchViewComponent },
  { path: "result-view/:id", component: ResultViewComponent },
  {
    path: "fav-view",
    component: FavViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
