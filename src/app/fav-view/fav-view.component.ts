import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OpenFoodFactProduct } from '../entity/open-food-fact-product.vo';
import { FavService } from '../services/fav.service';

@Component({
  selector: 'app-fav-view',
  templateUrl: './fav-view.component.html',
  styleUrls: ['./fav-view.component.css']
})
export class FavViewComponent implements OnInit {

  favProducts: Array<OpenFoodFactProduct> | undefined;

  constructor(private router: Router, private favService: FavService) {}

  ngOnInit() {
    this.favProducts = this.favService.getFavProducts();
    console.info("favProducts : ", this.favProducts)
  }

  favClickHandler(barCode: string): void {
    this.router.navigate(['/result-view/:codebar', { codebar: barCode }]);
  }
}
