import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchViewComponent } from './search-view/search-view.component';
import { ResultViewComponent } from './result-view/result-view.component';
import { FavViewComponent } from './fav-view/fav-view.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { OffService } from './services/off.service';
import { HttpClientModule } from '@angular/common/http';
import { FavService } from './services/fav.service';

@NgModule({
  declarations: [
    AppComponent,
    SearchViewComponent,
    ResultViewComponent,
    FavViewComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ZXingScannerModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ],
  providers: [OffService,FavService],
  bootstrap: [AppComponent]
})
export class AppModule { }
