import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OpenFoodFactProduct } from '../entity/open-food-fact-product.vo';

@Injectable({
  providedIn: 'root'
})
export class OffService {
  private URL_OFF : String = 'https://fr.openfoodfacts.org/api/v0/produit/{barcode}.json'

  constructor(private httpClient: HttpClient) {}

  async searchBarCode(barCode: string): Promise<OpenFoodFactProduct | undefined> {
    let url = this.URL_OFF.replace('{barcode}', barCode);
    return await this.httpClient
    .get(url)
    .toPromise().then(result=> {return result as OpenFoodFactProduct;})
  }
}
