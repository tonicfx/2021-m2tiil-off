import { Injectable } from '@angular/core';
import { Cookie } from 'ng2-cookies';
import { OpenFoodFactProduct } from '../entity/open-food-fact-product.vo';

@Injectable({
  providedIn: 'root'
})
export class FavService {
  constructor() {
    // Cookie.set('myopenfoodcookie', '');
  }

  public isFavProduct(product: OpenFoodFactProduct): boolean {
    let exist = false;
    if (product) {
      const products: Array<OpenFoodFactProduct> = this.getFavProducts();

      if (products) {
        products.forEach((productToTest: OpenFoodFactProduct) => {
          if (productToTest && product.code === productToTest.code) {
            exist = true;
          }
        });
      }
    }
    return exist;
  }

  public addFavProduct(product: OpenFoodFactProduct, add: boolean): void {
    if (!product) {
      return;
    }

    console.info("addFavProduct ", product)
    const products: Array<OpenFoodFactProduct> = this.getFavProducts();
    const finded: OpenFoodFactProduct | undefined = products.find(
      (p: OpenFoodFactProduct) => {
        return p.code === product.code;
      }
    );

    if (!finded && add) {
      products.push(product);
    } else if (finded && !add) {
      products.splice(products.indexOf(product), 1);
    }
    console.info("saveFavProducts ", products)
   
    this.saveFavProducts(products);
  }

  public getFavProducts(): Array<OpenFoodFactProduct> {
    const productSerialize: string = Cookie.get("myopenfoodcookie");
    console.info("productSerialize ", productSerialize);
    let products: Array<OpenFoodFactProduct> = [];

    if (productSerialize && productSerialize !== "") {
      products = JSON.parse(productSerialize);
    }
    return products;
  }

  private saveFavProducts(products: Array<OpenFoodFactProduct>): void {
    const productsToSave: Array<OpenFoodFactProduct> = [];
    products.forEach((product: OpenFoodFactProduct) => {
      productsToSave.push({
        product: {brand: product.product.brand},
        code: product.code,
      });
    });
    const productsSerialisation: string = JSON.stringify(productsToSave);
    
    Cookie.set("myopenfoodcookie", productsSerialisation);
 }
}
