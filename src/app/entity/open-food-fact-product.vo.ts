import { OpenFoodFactIngredient } from "./open-food-fact-ingredient.vo";

export interface OpenFoodFactProduct {
  code : string;
  
  product: any;

  fav?: boolean;
}
