export interface OpenFoodFactIngredient {
  id: string;

  text: string;

  rank: number;
}
